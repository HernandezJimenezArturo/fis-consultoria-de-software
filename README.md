## Mapa de La Consultoría

```plantuml
@startmindmap
*[#Violet] La Consultoría
**[#white] ¿Que es?
***[#Turquoise]: Empresa de servicios profesionales 
de alto valor añadido;
****_ nace
*****[#Turquoise]: gracias a la existencia de organizaciones
 que se convierten en clientes de esta;
******_ Las organizaciones
*******[#Turquoise]: no pueden operar y transformarse 
al mismo tiempo;
********_ por
*********[#white] Falta de personal calificado
*********[#white] Falta de conocimiento técnico
*********[#white] Falta de tiempo
****_ ofrece
*****[#white]: °Conocimiento Sectorial
 °Conocimiento Técnico
 °Capacidad de plantear soluciones;
******_: Ejemplo de 
empresa consultora;
*******[#Turquoise] AXPE consulting
********[#white]: Compañia multinacional de capital 100%
 español, de consultoría y tecnologias 
 de información;
 ********_ nació
 *********[#white] En 1998, España
 ********_ tiene
 *********[#white] Mas de 1600 profesionales a nivel mundial
**[#white] ¿Que exige?
***[#Turquoise] Aptitud
****_ los profesionales
*****[#white]: tienen que ser profesionales 
con una serie de aptitudes;
******_: un buen profesional
 se destaca por su;
*******[#Turquoise] Formación y experiencia
*******[#Turquoise] Capacidad de trabajo y evolución
******_: un buen profesional 
es capaz de;
*******[#Turquoise] Colaborar en un proyecto común 
*******[#Turquoise] Trabajar en equipo
*******[#Turquoise] Ofrecer honestidad y sinceridad
***[#Turquoise] Actitud
****[#white] Proactividad
*****[#Turquoise] Implica iniciativa y anticipación
*****[#Turquoise] Es imprescindible en consultoría
****[#white] Voluntad de mejora
*****[#Turquoise] Actitud activa en construcción de empresa
*****[#Turquoise] Aportación de ideas de mejora
****[#white] Responsabilidad
*****[#Turquoise] Asumir los trabajos para hacerlos
*****[#Turquoise] No devolver nuevos problemas
*****[#Turquoise] No esgrimir razones para no llevar a cabo el trabajo
***[#Turquoise] compromiso
****[#white] Disponibilidad total
*****[#Turquoise] Hay que estar dispuesto a trabajar
*****[#Turquoise]: Puede no encajar con algunas
 formnas de entender la vida;
****[#white] Viajes
*****[#Turquoise] Suele demandar movilidad geográfica
****[#white] Nuevos retos 
*****[#Turquoise] La variedad y los retos permiten evoluvionar mas rapido
*****[#Turquoise] Puede ser estresante pero nunca aburrido
**[#white] Carrera profesional
***[#Turquoise]: Todas las consultoras no pueden ser rigidas pues
 las carreras ofrecidas tienen que justarse a las
  necesidades del mercado;
****[#white] Modelo carrera
*****[#Turquoise] Modelo de categoria
*****[#Turquoise] Plan de carrera
*****[#Turquoise] Proceso de evaluación 
*****[#Turquoise] Plan de Formación
****[#white] Categorias profesionales
*****[#Turquoise]: °Junior
 °Senior
 °Gerente
 °Director;
******_ puestos en los ambitos
*******[#white] Consultoría
*******[#white] Aplicaciones
*******[#white] Infraestructuras TIC
*******[#white] BPO
****[#white] Plan de formación
*****[#Turquoise] Clientes
******_ eje
*******[#white]: °Comercial
 °sectorial;
*****[#Turquoise] Habilidades
******_ eje
*******[#white]: °Comunicación
°Gestión de personas
°Económico;
*****[#Turquoise] Productividad en delivery
******_ eje
*******[#white]: °Gestión del tiempo
°Project management
°Service management
°Metodología
°Calidad;
*****[#Turquoise] Tecnología
******_ eje
*******[#white]:] °Productividad técnica
°Formación Técnica especifica;
*****[#Turquoise] Idiomas
******_ eje
*******[#white]: °Inglés
°Francés;


left side

**[#white] Profesión a futuro
***[#Turquoise]: la consultoría es un modo de vida que
 permite alcanzar la felicidad profesional;
****_ la demanda
*****[#white] de consultoría era y es muy alta
****[#Turquoise] Tendencias
*****[#white]: °Transformación digital
°Industria conectada 4.0
°Smart cities;
******_ Requieren
*******[#Turquoise]: Una gran cantidad de profesionales
 que van a trabajar en diferentes 
 tipos de actividades;
********[#white]: Actividades de gestión
 intraempresa/interempresas;
 ********[#white]: Comunicaciones y
  tratamiento de datos;
 ********[#white]: Hibridación mundo 
 físico y digital;
**[#white] Servicios
***[#Turquoise] Consultoría
****[#white]: °Reingenieria de procesos
°Gobierto TI
°Definición de proyectos
°Analitica avanzada de datos;
*****_: Ejemplo 
       en 
    ambito;
******[#Turquoise] Social
*******[#white] Sentimental Analysis
******[#Turquoise] Mobility
*******[#white] Simplificación de procesos con movilidad
******[#Turquoise] Analytics
*******[#white] Desarrollo de modelos analíticos avanzados
******[#Turquoise] Cloud
*******[#white] Definición de estrategia clooud
***[#Turquoise] Integración
****[#white]: °Desarrollar a medida
°Aseguramiento de la calidad E2E
°Infraestructuras
°Soluciones de mercado;
*****_: Ejemplo 
       en 
    ambito;
******[#Turquoise] Social
*******[#white] Desarrollo de conectores con redes sociales
******[#Turquoise] Mobility
*******[#white] Desarrollo y prueba de APPS
******[#Turquoise] Analytics
*******[#white] Integración de fuentes
******[#Turquoise] Cloud
*******[#white] Implantación de estrategia cloud
***[#Turquoise] Externalización
****[#white]: °Gestión de Aplicaciones
°Servicios SOA
°Operacion y administración de Infraestructura;
*****_: Ejemplo 
       en 
    ambito;
******[#Turquoise] Social
*******[#white] Operación comunity Manager
******[#Turquoise] Mobility
*******[#white]: Operación de plataformas
 de aplicaciones moviles;
******[#Turquoise] Analytics
*******[#white]: Factoria de explotación y mantenimiento
de modelos analíticos;
******[#Turquoise] Cloud
*******[#white]: Operaciones de
 servicios cloud;
***[#Turquoise] De estrategia
****_: Estan mas ligados al
 ambito del negocio;
 *****[#white] Definición planes/modelos de negocios  
 *****[#white] Definición modelos organizativos y operativos
 *****[#white] Planes de medios
 *****[#white] Gestión de campañas
 *****[#white] Segmentación de clientes
@endmindmap
```
## Mapa de Consultoría de Software

```plantuml
@startmindmap
*[#lightgreen]: Consultoria 
de software;
**_: desarrollo
 de software;
***[#Orange]: Comienza cuando un cliente
  tiene la necesidad dentro 
   de su negocio;
****_: Proceso de desarrollo 
de software;
*****[#lightgreen] 1- Consultoria
******_ Se necesita
*******[#Orange]: Hablar con el cliente para ver
 que requisitos o requerrimientos 
 va a necesitar para el software a crear;
*****[#lightgreen] 1- Estudio de viabilidad
******_ analizar
*******[#Orange]: lo que el cliente quiere, que ventajas,
 que ahorro economico va a tener con el desarrollo de el software;
*****[#lightgreen] Diseño Funcional
******_ consiste
*******[#Orange]: En ver que información necesita ese 
sistema de información/ese software de entrada;
********_ produce
*********[#lightgreen] Información de entrada y salida
**********[#Orange] se almacena en base de datos
******[#Orange]: Se crea una especie
 de prototipo;
*******_ Para 
********[#lightgreen]: Que el cliente vea como va a 
ser ese diseño de pantalla, etc...;
*****[#lightgreen] Diseño Tecnico
******[#Orange] Programador
*******[#lightgreen]: traslada la necesidad a nivel tecnico
 con un lenguaje de programación;
********_ programadores
*********[#Orange]: Construyen en el lenguaje 
que el diseñador tecnico decida;
**********[#lightgreen]: despues realizan pruebas
 de lo que se construyo;
******[#Orange] Diseñador Tecnico
*******_ crea
********[#lightgreen] Todos los procesos y necesidades de dato
*******[#lightgreen] Hace pruebas integradas del software
*******[#lightgreen] Corrobora que se cumplan los requerimientos del cliente
*****[#lightgreen]: Si se aprueba el programa por el cliente se procede
 a implantar el programa en todos los ordenarores que el cliente quiera;
***[#Orange] Se crea un proyecto
****[#lightgreen]: El cliente decide si el desarrollo 
del programa se hace en sus instalaciones;
*****[#Orange]: El jefe de proyecto decide que 
personas tienen que estar en las instalaciones
 del cliente;
****_: si decide que se realice en 
instalaciones de la consultora;
*****[#Orange]: Se sigue un proyecto tradicional
 o factorias de software;

**[#Orange]: Tendencias en el desarrollo de 
un sistema de información;
***[#lightgreen]: Empiezan por cambiar el concepto de lo que
 antes era la infraestructura de una empresa;
****_ Nueva tendencia
*****[#Orange]: Almacenar información
 en la nube;
******[#lightgreen]: Con esto se ha cambiado 
la forma de tratar la información;
******[#lightgreen]: Se paga a empresas por capacidad
 de almacenimiento en la nube;
*******[#Orange] se registran en la nube millones y millones de datos diariamente
******[#lightgreen] Con la información en lanube se facilitan la vida y las empresas pueden saber de tus gustos;
**[#Orange] Consultora AXPE Consulting
***[#lightgreen]: Es una organización nacida
en madrid;
****[#Orange] Prestas sus servicios a 4 paises y 21 mas de manera indirecta
****[#Orange] Cuentan con 1500 profesionales 
****[#Orange]: desarrollan software a empresas
 y grandes organizaciones;
*****_ Orientadas al
******[#lightgreen]: uso intensivo de productos y servicios
 para conseguir la maxima eficiencia;
****[#Orange]: se dedican a implantar sistemas 
informaticos en grandes empresas;
***[#Orange]: Actividades desarrrolladas en
AXPE consulting;
****[#lightgreen]: Todas las tareas que permiten crear o mantener 
la informatica de una empresa;
****[#lightgreen]: Se da servicios no 
a particulares;
****[#Orange] ¿Que necesita una empresa para mantenerse?
*****[#lightgreen] Trabajar parte de la infraestructura
*****[#lightgreen] crear software para mejorar infraestructura
****[#Orange]: ¿Que hacer para que la infraestructura permanezca
 o si se cae como levantarlade nuevo?;
*****[#lightgreen] Dependera del volumen de la empresa
*****[#lightgreen] Monitorear todo el tiempo el software 
*****[#lightgreen] Soluciones de software
*****[#lightgreen] Trabajar con seguridad fiable
****_ Atiende
*****[#lightgreen] Grandes Empresas
******[#Orange]: tienen oficinas de proyecto que las 
atienden por tener mas proyectos;
*****[#lightgreen] Pequeñas Empresas
@endmindmap
```
## Mapa de Aplicación de la Ingenieria de Software

```plantuml
@startmindmap
*[#gold]: Aplicación de la
 Ingenieria de Software;
**_ Consultora Ejemplo
***[#FFBBCC]: Stratesys y su 
modelo de aplicación;
****_ ¿Que es?
*****[#white]: Empresa de servicios de tecnología 
que lleva en el mercado unos 15 años 
con capital 100% Español;
****[#white]: Expertos en 
soluciones APP;
*****_ ¿Que es APP?
******[#FFBBCC]: Es un software ya hecho que una 
 empresa utilizara para su beneficio;
*******[#white]: La labor de la consultora
 en proceso;
********[#FFBBCC]: Crea una serie de aplicaciones sobre el 
paquete estadar de la app en ABAP ó Java;
*********_ Para
**********[#white]: resolver una serie 
de tareas de temas puntuales;
*********_ enfoque
**********[#white]: se utiliza una 
metodología auditada;
***********[#FFBBCC]: Es bastante estandar 
dentro del mercado;
************[#white] FASE DE ANALISIS
*************[#FFBBCC] Documento de requisito
**************[#white]: La consultora 
reune requisitos;
***************[#FFBBCC] Lo que el cliente necesita
***************[#FFBBCC] Areas a aplicar la app
**************[#white]: La consultora se reune 
con todos los responsables 
de la empresa;
***************_ Para
****************[#FFBBCC]: tener una visión completa 
de todo lo que hace la empresa;
**************[#white] nivel de detalle
***************[#FFBBCC]: cada uno de los procesos
 que la empresa ejecuta en 
 su dia a dia;
****************[#white]: Con eso se indetifican sus subprocesos 
y sus tareas que realizan en la empresa;
*************_: Despues de este 
 documento sigue;
**************[#white]:    FASE DE 
ANALISIS FUNCIONAL;
***************[#FFBBCC]: Como la consultora creara 
el desarrollo del proyecto en base 
al requisito;
****************[#white]: Aquí se entrega el 
plan de proyecto;
*****************[#FFBBCC] El cliente tiene que aprobar el plan
***************_: aprobado este 
documento se pasa al;
****************[#white] FASE DE ANALISIS TECNICO
*****************[#FFBBCC]: El como se llevara a cabo cada 
tarea de manera tecnica;
******************_ Tanto
*******************[#white] Nivel de programación
*******************[#white] Parametrización
************[#white] FASE DE CONSTRUCCION
*************[#FFBBCC] Prototipado
**************[#white]: Primera versión de lo
 que el cliente va a tener;
***************[#FFBBCC]: Sirve para realimentación por parte
 del cliente si lo que va saliendo es
  lo que buscaba inicialmente;
*************[#FFBBCC]: Depende demasiado de
 la Fase de Analisis;
**************[#white]: Cualquier cosa que se modifique aqui,
 afectara todo el desarrollo;
************[#white] FASE DE PRUEBAS
************[#white] FASE DE CODIGO
*************_ Se divide en
**************[#FFBBCC] Consultores funcionales
***************[#white]: Se dedican a configurar
 esa herramienta;
**************[#FFBBCC] Programación
***************[#White]: Cada requisito funcional debe
 tener diseños técnicos;
****************[#FFBBCC]: Se documenta todo lo que se
 codifica y todos los cambios;
***************[#White]: El programador tiene que ser autonomo
  al hacer las primeras pruebas;
****************[#FFBBCC] Todas las pruebas igual se documentan  
***********[#FFBBCC]: Por un equipo de auditores 
internos como externos 
de la consultora;
************[#white] para conservar las certificaciones ISO
*************[#FFBBCC]: Todo lo escrito debe 
estar aprobado por el cliente;
*******[#white]: Todas las empresas 
tienen excepciones;
********_ Se cubren con
*********[#FFBBCC] programación
**********_: ¿Cuantos programadores se requieren
    para instalar la app en una
      empresa mediana?;
***********_ en promedio
************[#white] equipo de 7 a 8 personas
*************[#FFBBCC] Sección de programadores
*************[#FFBBCC] Seccion de consultores
**************[#white] Informaticos
**************[#white] Empresariales o de cualquier carrera
*******[#white]: Es un paquete que hay que instalar 
con una base de datos con 
un servidor de aplicación;
****[#white]: Tienen un enfoque muy claro
 hacia la metodología;
*****[#FFBBCC] Tienen dos certificados ISO
******[#white]: Que les exigen que la metodología 
se cumpla en todos los proyectos;
@endmindmap
```
